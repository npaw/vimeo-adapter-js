var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Vimeo = youbora.Adapter.extend({
  /** Override to return current plugin version */
  getVersion: function() {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return current playhead of the video */
  getPlayhead: function() {
    return this.playhead
  },

  /** Override to return current playrate */
  getPlayrate: function() {
    return this.playrate
  },

  /** Override to return video duration */
  getDuration: function() {
    return this.duration
  },

  /** Override to return rendition */
  getRendition: function() {
    player.getVideoWidth().then(function(width) {
      this.width = width
    }.bind(this)).catch(function(error) {
      youbora.Log.debug("unable to get width")
    }.bind(this));
    player.getVideoHeight().then(function(height) {
      this.height = height
    }.bind(this)).catch(function(error) {
      youbora.Log.debug("unable to get height")
    }.bind(this));
    if (this.width && this.height)
      return youbora.Util.buildRenditionString(this.width, this.height, 0)
    return null
  },

  /** Override to return title */
  getTitle: function() {
    return this.title
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function() {
    return false
  },

  /** Override to return resource URL. */
  getResource: function() {
    return this.url
  },

  /** Override to return player's name */
  getPlayerName: function() {
    return 'Vimeo'
  },

  /** Register listeners to this.player. */
  registerListeners: function() {
    // Enable playhead monitor (buffer = true, seek = false)
    this.monitorPlayhead(true, true)

    // References
    this.references = []
    this.references['play'] = this.playListener.bind(this)
    this.references['pause'] = this.pauseListener.bind(this)
    this.references['timeupdate'] = this.timeupdateListener.bind(this)
    this.references['error'] = this.errorListener.bind(this)
    this.references['seeked'] = this.seekedListener.bind(this)
    this.references['ended'] = this.endedListener.bind(this)
    this.references['playbackratechange'] = this.playbackListener.bind(this)
    this.references['loaded'] = this.newVideoListener.bind(this)
      // Register listeners
    for (var key in this.references) {
      this.player.on(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function() {
    // Disable playhead monitoring
    this.monitor.stop()

    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.off(key, this.references[key])
      }
      this.references = []
    }
  },

  /** Listener for 'play' event. */
  playListener: function(e) {
    this.duration = e.duration
    this.playhead = e.seconds
    this.fireStart()
    this.fireResume()
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function(e) {
    this.duration = e.duration
    this.playhead = e.seconds
    if (this.getPlayhead() > 0.1) {
      this.fireStart()
      this.fireJoin()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function(e) {
    this.duration = e.duration
    this.playhead = e.seconds
    this.firePause()
  },


  /** Listener for 'error' event. */
  errorListener: function(e) {
    this.fireError(e.name, e.message)
  },

  /** Listener for 'seeked' event. */
  seekedListener: function(e) {
    if (e.seconds === 0) {
      this.fireStop()
    }
    this.duration = e.duration
    this.playhead = e.seconds
  },

  /** Listener for 'bufferstart' event. */
  bufferingListener: function(e) {
    this.fireSeekBegin()
  },

  /** Listener for 'bufferend' event. */
  bufferedListener: function(e) {
    this.fireBufferEnd()
  },

  /** Listener for 'ended' event. */
  endedListener: function(e) {
    this.fireStop()
  },

  /** Listener for 'playbackratechange' event. */
  playbackListener: function(e) {
    this.playrate = e.playbackRate
  },

  /** Listener for 'loaded' event. */
  newVideoListener: function(e) {
    this.fireStop()
    this.player.getPlaybackRate().then(function(playbackRate) {
      this.playrate = playbackRate
    }.bind(this)).catch(function(error) {
      youbora.Log.debug("unable to get playrate")
    }.bind(this));
    player.getVideoTitle().then(function(title) {
      this.title = title
    }.bind(this)).catch(function(error) {
      youbora.Log.debug("unable to get title")
    }.bind(this));
    player.getVideoWidth().then(function(width) {
      this.width = width
    }.bind(this)).catch(function(error) {
      youbora.Log.debug("unable to get width")
    }.bind(this));
    player.getVideoHeight().then(function(height) {
      this.height = height
    }.bind(this)).catch(function(error) {
      youbora.Log.debug("unable to get height")
    }.bind(this));
    player.getVideoUrl().then(function(url) {
      this.url = url
    }.bind(this)).catch(function(error) {
      youbora.Log.debug("unable to get url")
    }.bind(this));
  }
})

module.exports = youbora.adapters.Vimeo